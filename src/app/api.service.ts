import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http:HttpClient) {}

  url = "https://Soportexcoope.pythonanywhere.com/";

  post_ticket(datos:any) {
    return this.http.post(this.url + "post_ticket", datos);
  }

  eliminar_ticket(id:Number) {
    return this.http.delete(this.url + "eliminar_ticket/" + id);
  }

  asignar_ticket(id:number, tecnico:string){
    let datos = {
      "tecnico": tecnico
    }
    return this.http.put(this.url + "asignar_ticket/" + id, datos)
  }

  traer_ticket(){
    return this.http.get <[]> (this.url + "traer_ticket")
  }

  traer_ticket_tecnico(id:any){
    return this.http.get <[]> (this.url + "traer_ticket_tecnico/" + id)
  }


  login(email: string, clave: string){
    let datos = {
      "email": email,
      "clave": clave
    }
    return this.http.post(this.url + "login", datos);
  }

  finalizar_ticket(id:number, datos:any){
    return this.http.put(this.url + "finalizar_ticket/" + id, datos)
  }


}