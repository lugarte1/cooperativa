import { Component } from '@angular/core';
import { ApiService } from "../api.service";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private servicio:ApiService, private nav:NavController) {}

  msjErrorLogin=""

  email = ""
  clave = ""
  
iniciar_sesion(){
  console.log(this.email)
  console.log(this.clave)
  this.servicio.login(this.email, this.clave).subscribe
  (respuesta=> {
    console.log(JSON.stringify(respuesta));
    let resp = JSON.parse(JSON.stringify(respuesta))
    if ( resp != null ){
      if (resp[0] == "profesor"){
        this.nav.navigateForward("/profesor/" + resp[1])
      }else if(resp[0] == "tecnico"){
        this.nav.navigateForward("/tecnico/" + resp[1])
      }else{
        console.log(this.msjErrorLogin)
        this.msjErrorLogin = resp["Resultado"]
      }
    }
  });
     
  }


}
  
