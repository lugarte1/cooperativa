import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.page.html',
  styleUrls: ['./profesor.page.scss'],
})
export class ProfesorPage implements OnInit {

  aula=""
  descripcion=""
  dispositivos=""
  profesor=""
  tecnico=""
  fecha_resuelto=""
  hora_resuelto=""

  constructor(private servicio: ApiService, private route:ActivatedRoute) { }

  EnviarDatos(){
    let Datos = {
      "aula": this.aula,
      "descripcion": this.descripcion,
      "dispositivos":this.dispositivos,
      "profesor":this.profesor,
      "tecnico":this.tecnico,
      "fecha_resuelto":this.fecha_resuelto,
      "hora_resuelto":this.hora_resuelto,
    }
    this.servicio.post_ticket(Datos).subscribe((Respuesta)=>{
      console.log(Respuesta);
    })

  }

  
    HacerAlgo(){
      //codigo 
    }

  ngOnInit() {
    this.route.params.subscribe(parametros=>{
      console.log(parametros)
      this.profesor = parametros["cuenta"]
    })
  }
    

}


