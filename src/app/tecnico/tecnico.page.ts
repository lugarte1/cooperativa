import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service'; 
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tecnico',
  templateUrl: './tecnico.page.html',
  styleUrls: ['./tecnico.page.scss'],
})
export class TecnicoPage implements OnInit {

  tickets=[]
  aula=""
  cuenta = ""

  constructor(private servicio: ApiService, private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params=>{
      console.log("params: ", params["cuenta"])
      this.cuenta = params["cuenta"]
    
    })
    this.servicio.traer_ticket().subscribe(respuesta=>{
      this.tickets = respuesta
      console.log(this.tickets)
    })
  }

  asignar(id:number){
    console.log(id)
    this.servicio.asignar_ticket(id, this.cuenta).subscribe(respuesta=>{
      console.log(respuesta)
    })

  }

}
