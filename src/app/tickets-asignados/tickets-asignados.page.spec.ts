import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TicketsAsignadosPage } from './tickets-asignados.page';

describe('TicketsAsignadosPage', () => {
  let component: TicketsAsignadosPage;
  let fixture: ComponentFixture<TicketsAsignadosPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsAsignadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
