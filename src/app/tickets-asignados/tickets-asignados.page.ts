import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-tickets-asignados',
  templateUrl: './tickets-asignados.page.html',
  styleUrls: ['./tickets-asignados.page.scss'],
})
export class TicketsAsignadosPage implements OnInit {

  tickets=[]
  cuenta=""

constructor(private activatedRoute:ActivatedRoute, private servicio:ApiService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(parametros=>{
      console.log(parametros)

      this.servicio.traer_ticket_tecnico(parametros["cuenta"]).subscribe(tickets=>{
        console.log(tickets)
        this.tickets = tickets
        this.cuenta = parametros["cuenta"]
      })

    })
  }

  finalizar_ticket(id_turno:number){
    console.log(id_turno)
    let datos={

    }
    this.servicio.finalizar_ticket(id_turno, datos).subscribe(respuesta=>{
      console.log(respuesta)
      location.href = "./" 
    })
  }

}
